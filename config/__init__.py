import os


class Config:
    PYTHON_PORT = "6081"
    DATA_HOME = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, 'data'))


# 读取环境变量,更新到Config后，以和容器一起使用
for k in os.environ:
    if k in Config.__dict__:
        #        logger.info(f'use environ var:{k},{os.environ.get(k)}')
        setattr(Config, k, os.environ.get(k))
CONFIG = Config()
for k in os.environ:
    if k in CONFIG.__dict__:
        #        logger.info(f'use environ var:{k},{os.environ.get(k)}')
        CONFIG.__dict__[k] = os.environ.get(k)
