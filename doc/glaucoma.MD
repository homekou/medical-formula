
# 模型介绍

  1. ![青光眼症状.png](../img/青光眼症状.png)
  2. ![青光眼诊断.png](../img/青光眼诊断.png)

# 青光眼评估调用说明
【请求路径】http://0.0.0.0:6001/api/p/medical/formula
【请求消息】
```
{
  "formula_name": "青光眼评估",
  "params": {
    "gender": 1,
    "symptom": ["眼肿瘤", "眼部炎症", "眼部严重创伤", "视力下降", "视力模糊", "灯光周围出现色圈", "眼睛发红", "眼睛剧烈疼痛"],
    "PDH": ["糖尿病", "高血压", "心脏病", "甲状腺功能减退症"],
    "MH": ["强的松", "糖皮质激素" "可的松"],
    "FDH": ["青光眼"],
    "HAC": ["高度近视", "远视", "玩手机、电脑、看电视", "关灯后看书"],
    "age":40
  }
}
```
【应答消息】
```
{
  {"青光眼评估结果": "需青光眼诊断/暂无青光眼诊断"},
  "result_code": 0
}


```