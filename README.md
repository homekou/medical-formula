# 医学公式

#### 介绍
收集医学权威计算公式

#### 软件架构

1.  计算每日所需最低卡路里（不同运动程度）
2.  计算bmi及bmi类型
3.  计算理想体重区间
4.  基于最低卡路里计算不同类别能量摄取量
5.  基于父母血型推测孩子血型


#### 安装教程

1.  pip install -r requirements.txt
2.  python main.api.py

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

# 附：标准化字段定义参考

为了便于多个公式见重用参数，建议尽量采用标准化字段定义

| 序号 | 字段  | 含义                                              | 类型 | 取值说明 |
| ---- | ------- | --------------------------------------------------- | ----- | ---------- |
| 1    | gender  | 性别（男=1，女=0）                                  | int   | 男=1，女=0 |
| 2    | age     | 年龄（年                                          | int   |            |
| 3    | smoke   | 吸烟状况(国标):1从不吸烟、2已戒烟、3吸烟               | int   |            |
| 4    | marital | 婚姻状况(国标):1未婚、2已婚、3丧偶、4离婚、5未知 | int   |            |
| 5    | alcohol | 饮酒状况(国标):1从不、2偶尔、3经常、4每天 | int   |            |
| 6    | sport   | 运动状况(国标):1每天、2每周一次以上、3偶尔、4不锻炼 | int   |            |
| 7    | height  | 身高 (cm)                                         | float |            |
| 8    | weight  | 体重 (kg)                                         | float |            |
| 9    | BMI     | 肥胖指数，体重(kg)除以身高(m)平方      | float |            |
| 10   | SBP     | 收缩压(mmHg)                                     | int   | 　        |
| 11   | DBP     | 舒张压(mmHg)                                     | int   | 　        |
| 12   | HbA1c   | 糖化血红蛋白(%)(中间是数字1不是字母l) | float | 　        |
| 13   | GLU     | 空腹血糖 (mmol/L)                               | float | 　        |
| 14   | TG      | 甘油三酯  (mmol/L)                              | float | 　        |
| 15   | TC      | 总胆固醇  (mmol/L)                              | float | 　        |
| 16   | LDL     | 低密度脂蛋白胆固醇  (mmol/L)               | float |            |
| 17   | HDL     | 高密度脂蛋白胆固醇 (mmol/L)                | float |            |
| 18   | HTN     | 是否高血压(hypertension)，来自高血压报卡数据 | int   | 否=0，是=1 |
| 19   | DM      | 是否糖尿病(diabetes mellitus )，来自高血压报卡数据 | int   | 否=0，是=1 |
| 20   | BF      | 体脂百分比 | float |            |
| 21   | ST      | 运动类别（细分）0久坐、1轻运动、2中等运动、3剧烈运动、3运动员 | int   |            |
| 22   | PDH     | 个人疾病史(Personal Disease history) | list   |            |
| 23   | FDH     | 家族疾病史(family Disease history) | list   |            |
| 24   | SC      | 血肌酐Serum creatinine（mmol/L） | float   |            |
| 25   | waist   | 腰围 | float   |            |
| 26   | TH      | 治疗历史 Treatment history | list   |            |
| 27   | MH      | 服用药物 Medication history | list  |            |
| 28   | environment | 生活环境 environment | list  |            |
| 29   | OCH | 职业接触史 Occupational contact history | list  |            |
| 30   | HAC | 生活习惯 habits and customs | list  |            |
| 31   | symptom | 症状 symptom | list  |            |

