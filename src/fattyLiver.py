#!/usr/bin/python
# encoding: utf-8
class fattyLiver():
    """
    脂肪肝评估
    """

    def __init__(self):
        """
        评估模型包含吸烟、喝酒、居住环境、家族疾病史、疾病史、运动、生活习惯6种因素
        """
        self.family_history = ["糖尿病"]
        self.disease_history = ["糖尿病"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        self.habits_and_customs = ["大量的肉类及（或）乳制品", "坐姿时间>=2小时"]
        self.review_history = ["从未测量空腹血糖", "从未测量胆固醇"]

    def calc(self, data):
        status = 0
        if data['gender'] == "1":
            status = status + 1
        my_age = int(data['age'])
        if my_age >= 50:
            status = status + 3
        elif 40 <= my_age < 50:
            status = status + 2
        elif 30 <= my_age < 40:
            status = status + 1
        bmi = pow(float(data['height']) / float(data['weight']), 2)
        if bmi >= 28:
            status = status + 2
        elif 24 <= bmi < 28:
            status = status + 1
        if 'waist' in data:
            if float(data['waist']) >= 85 and data['gender'] == '1':
                status = status + 1
            if float(data['waist']) >= 80 and data['gender'] == '0':
                status = status + 1
        if 'waist' in data and "hip" in data:
            if float(data['waist']) / float(data['hip']) >= 0.9 and data['gender'] == '1':
                status = status + 1
            if float(data['waist']) / float(data['hip']) >= 0.75 and data['gender'] == '0':
                status = status + 1
        if 'SBP' in data:
            if int(data['SBP']) >= 120:
                status = status + 1
        if 'DBP' in data:
            if int(data['DBP']) >= 90:
                status = status + 1
        if 'smoke' in data:
            if int(data['smoke']) == 2:
                status = status + 1
            if int(data['smoke']) == 3:
                status = status + 2
        if 'alcohol' in data:
            if int(data['alcohol']) >= 2:
                status = status + 1
        if 'HIAF' in data:
            status = status + 5 - int(data['HIAF'])
        if 'LMIAF' in data:
            status = status + 5 - int(data['LMIAF'])
        if 'FDH' in data:
            for element in data['FDH']:
                if element in self.family_history:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if 'HAC' in data:
            for element in data['HAC']:
                if element in self.habits_and_customs:
                    status = status + 1
        if 'RH' in data:
            for element in data['RH']:
                if element in self.review_history:
                    status = status + 1
        if status >= 16:
            return {"脂肪肝评估结果": "脂肪肝高风险人群"}
        elif 12 <= status <= 15:
            return {"脂肪肝评估结果": "脂肪肝中风险人群"}
        else:
            return {"脂肪肝评估结果": "脂肪肝低风险人群"}
