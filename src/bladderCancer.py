#!/usr/bin/python
# encoding: utf-8
class bladderCancer():
    """
    膀胱癌
    2020版《居民常见恶性肿瘤筛查和预防推荐》
    """

    def __init__(self):
        """
        评估模型包含吸烟、服用药物、家族疾病史、疾病史、治疗史、生活习惯、职业接触7种因素
        """
        self.family_history = ["膀胱癌"]
        self.disease_history = ["反复急慢性膀胱感染", "血吸虫引起的膀胱感染"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.occupation = ["油漆", "染料", "金属", "石油产品"]
        self.habits_and_customs = ["饮水中砷含量高", "饮用用氯处理过的水"]
        self.treatment = ["盆腔部位放射治疗", "使用导尿管"]
        self.drug = ["环磷酰胺或异环磷酰胺等抗癌药物", "含马兜铃酸的中草药"]

    def calc(self, data):
        status = 0
        if int(data['age']) >= 50:
            if 'FDH' in data:
                for element in data['FDH']:
                    if element in self.family_history:
                        status = status + 1
            if 'PDH' in data:
                for element in data['PDH']:
                    if element in self.disease_history:
                        status = status + 1
            if 'HAC' in data:
                for element in data['HAC']:
                    if element in self.habits_and_customs:
                        status = status + 1
            if 'MH' in data:
                for element in data['MH']:
                    if element in self.drug:
                        status = status + 1
            if 'OCH' in data:
                for element in data['OCH']:
                    if element in self.occupation:
                        status = status + 1
            if 'TH' in data:
                for element in data['TH']:
                    if element in self.treatment:
                        status = status + 1
            if status >= 1:
                return {"膀胱癌评估结果": "膀胱癌高危人群"}
            elif int(data['smoke']) == 3:
                return {"膀胱癌评估结果": "膀胱癌一般风险人群"}
            else:
                return {"膀胱癌评估结果": "膀胱癌低微风险人群"}
        else:
            return {"膀胱癌评估结果": "暂无膀胱癌风险人群"}
