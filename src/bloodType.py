#!/usr/bin/python
# encoding: utf-8

class bloodType():
    def __init__(self):
        """
        计算血型
        来源：https://baike.baidu.com/item/%E8%A1%80%E5%9E%8B%E9%81%97%E4%BC%A0%E8%A7%84%E5%BE%8B%E8%A1%A8/3442703?fr=aladdin
        """
        self.xue_xing_type = {"A": 0, "B": 1, "AB": 2, "O": 3}

    def calc(self, data):
        """
        计算血型
        :param data:{"父亲":"A","母亲":"A"}
        :return:{"孩子可能血型": ["O"], "孩子不可能血型": ["A", "AB", "B"]}
        """
        father = self.xue_xing_type[data['父亲']]
        mather = self.xue_xing_type[data['母亲']]
        if father == 3 and mather == 3:
            return {"孩子可能血型": ["O"], "孩子不可能血型": ["A", "AB", "B"]}
        elif father == 0 and mather == 3:
            return {"孩子可能血型": ["O", "A"], "孩子不可能血型": ["AB", "B"]}
        elif father == 0 and mather == 0:
            return {"孩子可能血型": ["O", "A"], "孩子不可能血型": ["AB", "B"]}
        elif father == 0 and mather == 1:
            return {"孩子可能血型": ["O", "A", "AB", "B"], "孩子不可能血型": []}
        elif father == 0 and mather == 2:
            return {"孩子可能血型": ["A", "AB", "B"], "孩子不可能血型": ["O"]}
        elif father == 1 and mather == 0:
            return {"孩子可能血型": ["B", "O"], "孩子不可能血型": ["A", "AB"]}
        elif father == 1 and mather == 1:
            return {"孩子可能血型": ["B", "O"], "孩子不可能血型": ["A", "AB"]}
        elif father == 1 and mather == 2:
            return {"孩子可能血型": ["A", "AB", "B"], "孩子不可能血型": ["O"]}
        elif father == 2 and mather == 3:
            return {"孩子可能血型": ["A", "B"], "孩子不可能血型": ["AB", "O"]}
        else:
            return {"孩子可能血型": ["A", "B", "AB"], "孩子不可能血型": ["O"]}
