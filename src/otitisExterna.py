#!/usr/bin/python
# encoding: utf-8
class otitisExterna():
    """
    外耳道炎评估
    https://www.baidu.com/bh/dict/ydxx_7983052008121370220?tab=%E6%A6%82%E8%BF%B0&title=%E5%A4%96%E8%80%B3%E9%81%93%E7%82%8E&contentid=ydxx_7983052008121370220&query=%E5%A4%96%E8%80%B3%E9%81%93%E7%82%8E&sf_ref=dict_home&from=dicta
    """

    def __init__(self):
        """
        评估模型包含服用药物史、疾病史、生活习惯3种因素
        """
        self.disease_history = ["糖尿病", "贫血", "内分泌紊乱", "外耳道炎"]
        self.habits_and_customs = ["佩戴耳机", "佩戴助听器", "挖耳朵不卫生", "水质不洁地方游泳"]
        self.drug = ["滴耳剂"]

    def calc(self, data):
        status = 0
        if 'MH' in data:
            for element in data['MH']:
                if element in self.drug:
                    status = status + 1
        if 'HAC' in data:
            for element in data['HAC']:
                if element in self.habits_and_customs:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if status >0:
            return {"外耳道炎评估结果": "外耳道炎高危人群"}
        else:
            return {"外耳道炎评估结果": "外耳道炎低风险人群"}
