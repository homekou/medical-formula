#!/usr/bin/python
# encoding: utf-8
class glaucoma():
    """
    青光眼评估
    https://www.drmed.cn/glaucoma/risk-factor
    """

    def __init__(self):
        """
        评估模型包含服用药物、家族疾病史、疾病史、生活习惯、症状5种因素
        """
        self.family_history = ["青光眼"]
        self.disease_history = ["糖尿病", "高血压", "心脏病", "甲状腺功能减退症"]
        self.signs_and_symptoms = ["眼肿瘤", "眼部炎症", "眼部严重创伤", "视力下降", "视力模糊", "灯光周围出现色圈", "眼睛发红", "眼睛剧烈疼痛"]
        self.drug = ["强的松", "糖皮质激素" "可的松"]
        self.habits_and_customs = ["高度近视", "远视", "玩手机、电脑、看电视", "关灯后看书"]

    def calc(self, data):
        status = 0
        if data['gender'] == 0 and data['age'] >= 40:
            status = status + 1
        elif data['age'] >= 60:
            status = status + 1
        if 'MH' in data:
            for element in data['MH']:
                if element in self.drug:
                    status = status + 1
        if 'symptom' in data:
            for element in data['symptom']:
                if element in self.signs_and_symptoms:
                    status = status + 1
        if 'FDH' in data:
            for element in data['FDH']:
                if element in self.family_history and int(data['age']) >= 35:
                    status = status + 1
        if 'HAC' in data:
            for element in data['HAC']:
                if element in self.habits_and_customs:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if status >= 1:
            return {"青光眼评估结果": "需青光眼诊断"}
        else:
            return {"青光眼评估结果": "暂无青光眼诊断"}
