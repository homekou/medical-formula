
import pickle
class FormulaBase:

  def calc(self,params):
    """
    处理数据
    """
    raise Exception('not implement')


class MyMLError(Exception):
    def __init__(self, result_code, message):
        # Call the base class constructor with the parameters it needs
        super(MyMLError, self).__init__(message)
        self.result_code = result_code
        self.message = message
