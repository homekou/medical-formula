#!/usr/bin/python
# encoding: utf-8
class leukemia():
    """
    白血病评估
    2020版《居民常见恶性肿瘤筛查和预防推荐》
    """

    def __init__(self):
        """
        评估模型包含吸烟、喝酒、服用药物、家族疾病史、疾病史、运动、bmi、治疗史、收缩压、舒张压10种因素
        """
        self.family_history = ["白血病"]
        self.disease_history = ["血液疾病", "自身免疫功能异常","白血病"]
        self.smoking = {"从不吸烟": 1, "已戒烟": 2, "吸烟": 3}
        self.drinking = {"从不": 1, "偶尔": 2, "经常": 3, "每天": 4}
        self.occupation = ["苯或含苯有机溶剂", "X射线、γ射线等电离辐射环境"]
        self.signs_and_symptoms = ["无诱因慢性出血", "全身无力", "全身疲倦并伴有骨关节疼痛"]
        self.habits_and_customs = ["吸毒"]

    def calc(self, data):
        status = 0
        if 'FDH' in data:
            for element in data['FDH']:
                if element in self.family_history:
                    status = status + 1
        if 'PDH' in data:
            for element in data['PDH']:
                if element in self.disease_history:
                    status = status + 1
        if status >= 1 or list(set(data['HAC']).intersection(set(self.habits_and_customs))) != [] or list(
                set(data['symptom']).intersection(set(self.signs_and_symptoms))) != [] or list(
            set(data['environment']).intersection(set(self.occupation))) != []:
            return {"白血病评估结果": "白血病高危人群"}
        elif int(data['alcohol']) == 4 and int(data['smoke']) == 3:
            return {"白血病评估结果": "白血病中危人群"}
        else:
            return {"白血病评估结果": "白血病低危人群"}
