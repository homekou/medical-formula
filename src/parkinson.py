#!/usr/bin/python
# encoding: utf-8
class parkinson():
    """
    帕金森病评估
    https://www.baidu.com/bh/dict/ydxx_8125335598851566544?tab=%E6%A6%82%E8%BF%B0&title=%E5%B8%95%E9%87%91%E6%A3%AE%E7%97%85&contentid=ydxx_8125335598851566544&query=%E5%B8%95%E9%87%91%E6%A3%AE%E7%97%85&sf_ref=dict_home&from=dicta
    """

    def __init__(self):
        """
        评估模型包含家族疾病史、症状、职业接触3种因素
        """
        self.family_history = ["帕金森病"]
        self.symptom = ["姿势平衡障碍、记忆力下降伴抑郁、运动迟缓"]
        self.occupation = ["杀虫剂", "除草剂", "鱼藤酮"]

    def calc(self, data):
        status = 0
        if 'FDH' in data:
            for element in data['FDH']:
                if element in self.family_history:
                    status = status + 1
        if 'OCH' in data:
            for element in data['OCH']:
                if element in self.occupation:
                    status = status + 1
        if status >= 1:
            return {"帕金森病评估结果": "帕金森病高危人群"}
        elif int(data['age']) >= 60:
            return {"帕金森病评估结果": "帕金森病低危人群"}
        else:
            return {"帕金森病评估结果": "暂不符合帕金森病筛查人群"}
