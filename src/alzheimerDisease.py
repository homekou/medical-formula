#!/usr/bin/python
# encoding: utf-8
class alzheimerDisease():
    """
    阿尔兹海默症评估
    http://disease.medlive.cn/wiki/entry/10001084_402_0
    https://wenku.baidu.com/view/98ad26f6ce22bcd126fff705cc17552707225e91.html
    http://disease.medlive.cn/wiki/entry/10001084_401_0
    """

    def __init__(self):
        """
        评估模型包含家族疾病史、疾病史、症状3种因素
        """
        self.family_history = ["痴呆", "阿尔兹海默症"]
        self.disease_history = ['唐氏综合症', '脑血管病', '高脂血症']
        self.symptoms = ["脑外伤", "记忆力下降", "失语", "迷失方向", "日常生活能力下降", "人格改变", "淡漠", "定向力障碍", "概念性失用", "情绪变化"]

    def calc(self, data):
        status = 0
        if int(data['age']) >= 65:
            if 'EH' in data:
                if int(data['EH']) <= 8:
                    status = status + 1
            if 'HCY' in data:
                if int(data['HCY']) >= 10:
                    status = status + 1
            if 'FDH' in data:
                for element in data['FDH']:
                    if element in self.family_history:
                        status = status + 1
            if 'PDH' in data:
                for element in data['PDH']:
                    if element in self.disease_history:
                        status = status + 1
            if status >= 1:
                return {"阿尔兹海默症评估结果": "阿尔兹海默症高危人群"}
            else:
                return {"阿尔兹海默症评估结果": "阿尔兹海默症低微风险人群"}
        else:
            return {"阿尔兹海默症评估结果": "暂无阿尔兹海默症风险人群"}
