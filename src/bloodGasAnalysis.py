#!/usr/bin/python
# encoding: utf-8
class bloodGasAnalysis():
    """
    血气指标分析
    https://wenku.baidu.com/view/fb27e8ec2aea81c758f5f61fb7360b4c2e3f2a8d.html?fr=search-1_income5
    """

    def __init__(self):
        pass

    def calc(self, data):
        result = []
        if "PH" in data:
            PH = float(data['PH'])
            if "体温" in data:
                PH = PH + 0.0147 * (37 - float(data['体温']))
            if PH < 7.35:
                result.append("疑似酸血症")
            elif PH > 7.45:
                result.append("疑似碱血症")
            elif 7.35 <= PH <= 7.45:
                result.append("PH值正常")
        if "PCO2" in data:
            if int(data['PCO2']) <= 35:
                result.append("疑似低碳酸血症")
            elif int(data['PCO2']) >= 45:
                result.append("疑似高碳酸血症")
            elif 35 <= int(data['PCO2']) <= 45:
                result.append("PCO2值正常")
        if "PO2" in data:
            if int(data['PO2']) <= 30:
                result.append("疑似生命危险")
            elif int(data['PO2']) <= 55:
                result.append("疑似提示呼吸衰竭")
            elif 83 <= int(data['PO2']) <= 108:
                result.append("PO2值正常")
        if "PSO2" in data:
            if 95 <= int(data['PSO2']) <= 98:
                result.append("PSO2值正常")
        if "HCT" in data:
            if 35 <= int(data['HCT']) <= 49:
                result.append("HCT值正常")
        if "HB" in data:
            if 110 <= int(data['HB']) <= 160:
                result.append("HB值正常")
        return {"分析结果": result}
